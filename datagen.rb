require 'date'
require 'csv'
require 'securerandom'

project_count = 4
affected_users = 50.times.map do |i|
  "user#{i}"
end

payload = %({"sdk":{"name":"sentry.ruby","version":"5.2.1"},"tags":{},"user":{},"extra":{},"level":"error","message":"","modules":{"byebug":"11.1.3","bundler":"2.3.8","sentry-ruby":"5.2.1","concurrent-ruby":"1.1.10","sentry-ruby-core":"5.2.1"},"threads":{"values":[{"id":880,"name":null,"crashed":true,"current":true,"stacktrace":null}]},"contexts":{"os":{"name":"Linux","build":"5.14.14-gentoo-dist","version":"#1 SMP Sat Oct 23 17:01:05 CEST 2021","kernel_version":"#1 SMP Sat Oct 23 17:01:05 CEST 2021"},"runtime":{"name":"ruby27","version":"ruby 2.7.5p203 (2021-11-24 revision f69aeb8314) [x86_64-linux]"}},"event_id":"4df121cdf82a43b2aa81e9ccce01dc0b","platform":"ruby","exception":{"values":[{"type":"RuntimeError","value":"foo","module":"","thread_id":880,"stacktrace":{"frames":[{"in_app":false,"lineno":18,"abs_path":"foo.rb","filename":"foo.rb","function":"\u003cmain\u003e","pre_context":["end\n","\n","begin\n"],"context_line":"b\n","post_context":["rescue StandardError =\u003e ex\n","  Sentry.capture_exception(ex)\n","end\n"],"project_root":"/home/ahegyi/dev/error_test"},{"in_app":false,"lineno":11,"abs_path":"foo.rb","filename":"foo.rb","function":"b","pre_context":["end\n","\n","def b\n"],"context_line":"  raise 'foo'\n","post_context":["end\n","def a\n","  b\n"],"project_root":"/home/ahegyi/dev/error_test"}]}}]},"timestamp":"2022-04-08T10:58:52Z","breadcrumbs":{"values":[]},"environment":"production","fingerprint":[],"server_name":"ahegyi-gitlab"})
users = (0..100)
error_count_per_project = (0..300)
error_count_per_project_outlier = (0..10_000)
event_count_per_project = (0..500)
event_count_per_project_outlier = (0..1_000_000)
outlier_ratio = 0.01
resolved_unresolved_ratio = 0.05
ignored_ratio = 0.05
date_range = (Date.new(2021, 6, 1).to_time.to_i..Date.today.to_time.to_i)
names = %w[ZeroDivisionError NameError Error StandardError TypeError KeyError]
environments = %w[staging production test prd gprd stg gstg]
actors = [
  'to_cart_form_alert()',
  '_next(grpc._channel)',
  'get(java.net.InetAddress)',
  'newInstance'
]
platforms = %w[ruby python java]
descriptions = [
  'PostsController#error',
  'undefined local variable or method undefined_method for #<PostsController:0x00007fe48c4e4238>',
  'a bytes-like object is required'
]

events = CSV.open('events.csv', 'w')
status = CSV.open('status.csv', 'w')
ignored = CSV.open('ignored.csv', 'w')

fingerprint = 0
project_count.times do |project_id|
  puts "project: #{project_id}"

  error_count = rand < outlier_ratio ? rand(error_count_per_project_outlier) : rand(error_count_per_project)
  puts "generating: #{error_count} errors"

  error_count.times do
    affected_user_count = rand(affected_users.size)
    fingerprint += 1

    event_count = rand < outlier_ratio ? rand(event_count_per_project_outlier) : rand(event_count_per_project)
    puts "generating: #{event_count} events"

    event_count.times do
      events << [
        project_id,
        fingerprint,
        names.sample,
        descriptions.sample,
        actors.sample,
        environments.sample,
        platforms.sample,
        '',
        affected_users.first(affected_user_count).sample,
        payload,
        Time.at(rand(date_range)).strftime('%Y-%m-%d %H:%M:%S')
      ]
    end

    if rand < resolved_unresolved_ratio
      status << [
        project_id,
        fingerprint,
        2,
        rand(users),
        1,
        Time.now.strftime('%Y-%m-%d %H:%M:%S')
      ]
    else
      status << [
        project_id,
        fingerprint,
        1,
        nil,
        2,
        Time.now.strftime('%Y-%m-%d %H:%M:%S')
      ]
    end

    if rand < ignored_ratio
      ignored << [
        project_id,
        fingerprint,
        rand(users),
        Time.now.strftime('%Y-%m-%d %H:%M:%S')
      ]
    end
  end
end

