swagger: "2.0"
info:
  description: "This schema describes the API endpoints for the error tracking feature"
  version: "0.0.1"
  title: "Error Trackig REST API"
host: "localhost"
basePath: "/api/v1"
tags:
- name: "errors"
  description: "Related to the error objects"
- name: "events"
  description: "Related to the error event objects"
- name: "projects"
  description: "Related to the project objects"
schemes:
- "https"
- "http"
paths:
  /projects/{projectId}/errors:
    get:
      tags:
      - "errors"
      summary: "List of errors"
      description: ""
      operationId: "listErrors"
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "int64"
      - name: "sort"
        in: "query"
        type: "string"
        default: "last_seen_desc"
        enum:
        - "last_seen_desc"
        - "first_seen_desc"
        - "frequency_desc"
      - name: "status"
        in: "query"
        type: "string"
        default: "unresolved"
        enum:
        - "unresolved"
        - "resolved"
        - "ignored"
      - name: "query"
        in: "query"
        type: "string"
      - name: "cursor"
        in: "query"
        type: "string"
        description: "Base64 encoded information for pagination"
      - name: "limit"
        description: "Number of entries to return"
        in: "query"
        type: "integer"
        default: 20
        minimum: 1
        maximum: 100
      responses:
        "200":
          description: "Success"
          headers:
            Link:
              type: string
              description: Link header containing pagination information (previous and next pages). https://docs.gitlab.com/ee/api/#pagination-link-header
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Error"
        "404":
          description: "Error not found"
  /projects/{projectId}/errors/{fingerprint}:
    get:
      tags:
      - "errors"
      summary: "Get information about the error"
      description: ""
      operationId: "getError"
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "int64"
      - name: "fingerprint"
        in: "path"
        description: "ID of the error that needs to be updated deleted"
        required: true
        type: "integer"
        format: "int64"
      responses:
        "200":
          description: "Success"
          schema:
            $ref: "#/definitions/Error"
        "404":
          description: "Error not found"
    put:
      tags:
      - "errors"
      summary: "Update the status of the error"
      description: ""
      operationId: "updateError"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "int64"
      - name: "fingerprint"
        in: "path"
        description: "ID of the error that needs to be updated deleted"
        required: true
        type: "integer"
        format: "int64"
      - in: "body"
        name: "body"
        description: "Error update object with the new values"
        required: true
        schema:
          $ref: "#/definitions/ErrorUpdatePayload"
      responses:
        "404":
          description: "Error not found"
        "200":
          description: "Success"
          schema:
            $ref: "#/definitions/Error"
  #
  # Ingestion endpoints required by the Sentry API
  #
  # There is a reason why we have such uncommon path. We depend on a client side
  # error tracking software which modifies URL for its own reasons.
  #
  # When we give user a URL like this:
  #   HOST/api/v1/projects/123
  #
  # Then error tracking software will convert it like this:
  #   HOST/api/v1/projects/api/123/store/
  #   HOST/api/v1/projects/api/123/envelope/
  #
  /projects/api/{projectId}/store:
    post:
      tags:
      - "events"
      summary: "Ingestion endpoint for error events sent from client SDKs"
      description: ""
      #
      # operationId is an optional unique string used to identify an operation.
      # If provided, these IDs must be unique among all operations described in
      # your API.
      #
      # Don't set operation Id so we can $ref this for the envelope endpoint
      # operationId: "createEvent". This means the code generator will give pick
      # a name for the method, example for the go code:
      #     events.NewPostProjectsProjectIDEnvelope
      #     events.NewPostProjectsProjectIDStore
      #
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "int64"
      responses:
        # Some sentry clients sdk require status 200 OK to work correctly.
        # See https://gitlab.com/gitlab-org/gitlab/-/issues/343531.
        "200":
          description: "Accepted. Event will be created async"
          schema:
            $ref: "#/definitions/ErrorEvent"
        "400":
          description: "Bad request"
        "500":
          description: "Internal error"
  /projects/api/{projectId}/envelope:
    post:
      tags:
      - "events"
      summary: "Ingestion endpoint for error events sent from client SDKs"
      description: ""
      #
      # operationId is an optional unique string used to identify an operation.
      # If provided, these IDs must be unique among all operations described in
      # your API.
      #
      # Don't set operation Id so we can $ref this for the envelope endpoint
      # operationId: "createEvent". This means the code generator will give pick
      # a name for the method, example for the go code:
      #     events.NewPostProjectsProjectIDEnvelope
      #     events.NewPostProjectsProjectIDStore
      #
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "int64"
      responses:
        # Some sentry clients sdk require status 200 OK to work correctly.
        # See https://gitlab.com/gitlab-org/gitlab/-/issues/343531.
        "200":
          description: "Accepted. Event will be created async"
          schema:
            $ref: "#/definitions/ErrorEvent"
        "400":
          description: "Bad request"
        "500":
          description: "Internal error"

  /projects/{projectId}/errors/{fingerprint}/events:
    get:
      tags:
      - "errors"
      - "events"
      summary: "Get information about the events related to the error"
      description: ""
      operationId: "listEvents"
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "int64"
      - name: "fingerprint"
        in: "path"
        description: "ID of the error within the project"
        required: true
        type: "integer"
        format: "int64"
      - name: "sort"
        in: "query"
        type: "string"
        default: "occurred_at_asc"
        enum:
        - "occurred_at_asc"
        - "occurred_at_desc"
      - name: "cursor"
        in: "query"
        type: "string"
        description: "Base64 encoded information for pagination"
      - name: "limit"
        description: "Number of entries to return"
        in: "query"
        type: "integer"
        default: 20
        minimum: 1
        maximum: 100
      responses:
        "200":
          description: "Success"
          headers:
            Link:
              type: string
              description: Link header containing pagination information (previous and next pages). https://docs.gitlab.com/ee/api/#pagination-link-header
          schema:
            type: "array"
            items:
              $ref: "#/definitions/ErrorEvent"
        "404":
          description: "Error not found"
  /projects/{id}:
    delete:
      summary: "Deletes all project related data. Mostly for testing purposes and later for production to clean updeleted projects."
      description: ""
      operationId: "deleteProject"
      tags:
      - "projects"
      parameters:
      - name: "id"
        in: "path"
        description: "ID of the project"
        required: true
        type: "integer"
        format: "int64"
      responses:
        "404":
          description: "Error not found"
        "201":
          description: "Success, delete will happen async."
definitions:
  Error:
    type: "object"
    properties:
      fingerprint:
        type: "integer"
        format: "int64"
      project_id:
        type: "integer"
        format: "int64"
      name:
        type: "string"
        example: "ActionView::MissingTemplate"
      description:
        type: "string"
        example: "Missing template posts/edit"
      actor:
        type: "string"
        example: "PostsController#edit"
      event_count:
        type: "integer"
        format: "int64"
      approximated_user_count:
        type: "integer"
        format: "int64"
      last_seen_at:
        type: "string"
        format: "date-time"
      first_seen_at:
        type: "string"
        format: "date-time"
      status:
        type: "string"
        description: "Status of the error"
        enum:
        - "unresolved"
        - "resolved"
        - "ignored"
  ErrorUpdatePayload:
    type: "object"
    properties:
      status:
        type: "string"
        description: "Status of the error"
        enum:
        - "unresolved"
        - "resolved"
        - "ignored"
      updated_by_id:
        type: "integer"
        description: "GitLab user id who triggered the update"
  ErrorEvent:
    type: "object"
    properties:
      fingerprint:
        type: "integer"
        format: "int64"
      projectId:
        type: "integer"
        format: "int64"
      payload:
        type: "string"
        description: "JSON encoded string"
      name:
        type: "string"
        example: "ActionView::MissingTemplate"
      description:
        type: "string"
        example: "Missing template posts/edit"
      actor:
        type: "string"
        example: "PostsController#edit"
      environment:
        type: "string"
        example: "production"
      platform:
        type: "string"
        example: "ruby"
