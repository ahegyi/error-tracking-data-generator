# Error Tracking data generator 

## Generate data

```ruby
ruby datagen.rb
```

## Create ClickHouse schema 

```bash
schema.sql | clickhouse-client -h 127.0.0.1 -mn --password
```

## Import data

```bash
cat events.csv | clickhouse-client -h 127.0.0.1 --password -q 'insert into error_tracking_error_events format CSV'
cat status.csv | clickhouse-client -h 127.0.0.1 --password -q 'insert into error_tracking_error_status format CSV'
cat ignored.csv | clickhouse-client -h 127.0.0.1 --password -q 'insert into error_tracking_ignored_errors format CSV'
```

## Example queries

Notes: 

- The queries will work properly if the [join_use_nulls setting](https://clickhouse.com/docs/en/operations/settings/settings#join_use_nulls) is enabled.
- All queries will include `project_id` filter. We don't plan to have cross project queries.
- We might have `JOIN-s` on two columns: `project_id`, `fingerprint`.

### Show unresolved (and not ignored) errors, ordered by event count:

This query loads N records (max 20) for the main page. The page is requested infrequently, only when the user visits the page.

Note: to avoid the complex `GROUP BY` query, we could use the [`FINAL` modifier](https://clickhouse.com/docs/en/sql-reference/statements/select/from/#select-from-final), however it could cause writes query time (merges parts).

Various sorting options are supported:

- `last_seen_at`
- `first_seen_at`
- `event_count`

The following query shows records for the first page. The second `ORDER BY` column (`fingerprint`) is a tie-breaker in case we encounter the same value for the event count. The data is keyset-paginated on the UI.

```sql
SELECT error_tracking_errors.*, COALESCE(error_tracking_error_status.status, 1) AS status FROM (
  SELECT 
    project_id,
    fingerprint,
    any(name) as name,
    sum(event_count) as event_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at,
    uniqMerge(approximated_user_count) as approximated_user_count
  FROM error_tracking_errors
  WHERE project_id = 1
  GROUP BY project_id, fingerprint
) as error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM error_tracking_error_status
  GROUP BY project_id, fingerprint
  HAVING status = 0
) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND
  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint
LEFT JOIN ( -- this could be a NOT EXISTS subquery
  SELECT project_id, max(updated_at) as updated_at, fingerprint
  FROM error_tracking_ignored_errors    
  GROUP BY project_id, fingerprint
) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND
  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint
WHERE
  error_tracking_ignored_errors.project_id IS NULL
ORDER BY error_tracking_errors.event_count DESC, error_tracking_errors.fingerprint DESC
LIMIT 20
```

### Resolve an error (by the user)

- We set the `user_id` so we know who was the actor.
- In some cases the `user_id` will be NULL because the status change was done by the system: error was resolved and it changes to unresolved when a new event occurs.
- Another case: user is removed from the system, we'll probably nullify the `user_id` column.

```sql
INSERT INTO 
error_tracking_error_status 
(project_id, fingerprint, status, user_id, actor, updated_at) 
VALUES (2, '331badc1-c256-4daa-860f-52d911d155e5', 2, 12, 1, now('UTC'));
```

### Process a new event

We expect large volume of events. To process a new event, we insert 2 records:

- `events` table, raw event with the payload.
- `status` table to "unresolve" and error. Actor: 2 (system), user_id: NULL

Event insert query:
                                         
```sql
INSERT INTO 
error_tracking_error_events 
(project_id, fingerprint, name, description, actor, environment, platform, level, payload, occurred_at) 
VALUES (2, '331badc1-c256-4daa-860f-52d911d155e5', 'TypeError', 'TypeError', 'Ruby', 'Ruby', 'production', 'error', '{"a": 1}', now('UTC'));
```                                      

Error status insert query:

```sql
INSERT INTO
error_tracking_error_status
(project_id, fingerprint, status, user_id, actor, updated_at)
VALUES (2, '331badc1-c256-4daa-860f-52d911d155e5', 2, NULL, 2, now('UTC'));
```

### Mark an error ignored

An error is ignored if there is a corresponding record in the `error_tracking_ignored_errors` table:

```sql
INSERT INTO 
error_tracking_ignored_errors 
(project_id, fingerprint, user_id, updated_at) 
VALUES (2, '331badc1-c256-4daa-860f-52d911d155e5', 25, now('UTC'));
```                                      

This data will be used on the UI when showing the not ignored errors by default. The user will have an option to list the ignored errors and unignore them.

### Show the first event for a fingerprint

```ruby
SELECT * FROM error_tracking_error_events
WHERE project_id = 2 AND fingerprint = '331badc1-c256-4daa-860f-52d911d155e5'
ORDER BY occurred_at ASC 
LIMIT 1;
```

### Show the last event for a fingerprint

```ruby
SELECT * FROM error_tracking_error_events
WHERE project_id = 2 AND fingerprint = '331badc1-c256-4daa-860f-52d911d155e5'
ORDER BY occurred_at DESC 
LIMIT 1;
```
