DROP TABLE IF EXISTS error_tracking_error_events;
DROP TABLE IF EXISTS error_tracking_errors;
DROP VIEW IF EXISTS error_tracking_errors_mv;
DROP TABLE IF EXISTS error_tracking_error_status;
DROP TABLE IF EXISTS error_tracking_ignored_errors;

CREATE TABLE error_tracking_error_events(
  project_id UInt64,
  fingerprint UInt64,
  name String,
  description String,
  actor String,
  environment LowCardinality(String),
  platform String,
  level LowCardinality(String),
  user_identifier String,
  payload String CODEC(LZ4HC(9)), -- AVG 20K bytes
  occurred_at DateTime('UTC')
) ENGINE = MergeTree PARTITION BY toYYYYMM(occurred_at)
ORDER BY (project_id, fingerprint, occurred_at);

CREATE TABLE error_tracking_errors
(
    project_id UInt64,
    fingerprint UInt64,
    name String,
    description String,
    actor String,
    event_count UInt64,
    approximated_user_count AggregateFunction(uniq, String),
    last_seen_at SimpleAggregateFunction(max, DateTime('UTC')),
    first_seen_at SimpleAggregateFunction(min, DateTime('UTC'))
)
ENGINE = SummingMergeTree
ORDER BY (project_id, fingerprint);

CREATE MATERIALIZED VIEW error_tracking_errors_mv TO error_tracking_errors
AS
SELECT
    project_id,
    fingerprint,
    any(name) AS name,
    any(description) AS description,
    any(actor) AS actor,
    count() AS event_count,
    uniqState(user_identifier) AS approximated_user_count,
    max(occurred_at) AS last_seen_at,
    min(occurred_at) AS first_seen_at
FROM error_tracking_error_events
GROUP BY
    project_id,
    fingerprint;

CREATE TABLE error_tracking_error_status
(
  project_id UInt64,
  fingerprint UInt64,
  status UInt8, -- 0 unresolved, 1 resolved
  user_id UInt64,
  actor UInt8, -- 0 status changed by user, 1 status changed by system (new event happened after resolve)
  updated_at DateTime('UTC')
) ENGINE = ReplacingMergeTree(updated_at)
ORDER BY (project_id, fingerprint);

CREATE TABLE error_tracking_ignored_errors
(
  project_id UInt64,
  fingerprint UInt64,
  user_id Nullable(UInt64) DEFAULT NULL,
  updated_at DateTime('UTC')
) ENGINE = ReplacingMergeTree(updated_at)
ORDER BY (project_id, fingerprint);

